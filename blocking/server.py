import sys
from socket import socket


srv = socket()

host = ''
port = 0 if len(sys.argv) < 2 else int(sys.argv[1])
srv.bind((host, port))
print(f'server socket: {srv.getsockname()}')

srv.listen(1)
while True:
    print('awaiting connection')
    cli, (host, port) = srv.accept()
    print(f'got connection: {host}:{port}')
    msg = cli.recv(1024)
    print(f'got message {msg}, sending resp...')
    cli.sendall(b'thanks for: ' + msg)
