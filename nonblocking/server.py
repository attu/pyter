from socket import socket
from select import select
import sys

srv = socket()
srv.setblocking(0)

host = ''
port = 0 if len(sys.argv) < 2 else int(sys.argv[1])
srv.bind((host, port))
print(f'server socket: {srv.getsockname()}')
srv.listen(5)


inputs = {srv}
outputs = set()
outgoing = {}

while inputs:
    readable, writeable, errors = select(inputs, outputs, set())

    for s in readable:
        if s is srv:
            cli, (host, port) = s.accept()
            print(
                f'connection from {host}:{port} accped, got {len(inputs)} connections')
            cli.setblocking(0)
            inputs.add(cli)
            outgoing[cli] = []
        else:
            msg = s.recv(1024)
            if msg:
                print(f'got message "{msg}" from {s.getpeername()}')
                outgoing[s].append(b'thanks for ' + msg)
                outputs.add(s)
            else:
                print(f'connection with {s.getpeername()} is closed')
                inputs.discard(s)
                outputs.discard(s)
                for omsg in outgoing[s]:
                    print(
                        f'outgoing message "{omsg}" for {s.getpeername()} is being dropped')
                del outgoing[s]
                s.close()

    for s in writeable:
        if outgoing[s]:
            msg = outgoing[s].pop(0)
            print(f'sending "{msg}" to {s.getpeername()}')
            s.sendall(msg)
        else:
            outputs.discard(s)

    for s in errors:
        print(f'connection with {s.getpeername()} has error')
        inputs.discard(s)
        outputs.discard(s)
        for omsg in outgoing[s]:
            print(
                f'outgoing message "{omsg}" for {s.getpeername()} is being dropped')
        del outgoing[s]
        s.close()
