from socket import socket
from select import select
import sys
from concurrent.futures import ThreadPoolExecutor

srv = socket()
srv.setblocking(0)

host = ''
port = 0 if len(sys.argv) < 2 else int(sys.argv[1])
srv.bind((host, port))
print(f'server socket: {srv.getsockname()}')
srv.listen(5)


inputs = {srv}
outgoing = {}


def handle_connection(sock, msg):
    while msg:
        print(f'sending "{msg}" to {sock.getpeername()}')
        sock.sendall(msg)
        msg = sock.recv(1024)
    print(f'connection with {s.getpeername()} is closed')
    sock.close()


with ThreadPoolExecutor(max_workers=6) as pool:
    while inputs:
        readable, _, errors = select(inputs, [], set())

        for s in readable:
            if s is srv:
                cli, (host, port) = s.accept()
                print(
                    f'connection from {host}:{port} accped, got {len(inputs)} connections')
                cli.setblocking(0)
                inputs.add(cli)
            else:
                msg = s.recv(1024)
                inputs.discard(s)
                if msg:
                    print(f'got message "{msg}" from {s.getpeername()}')
                    s.setblocking(True)
                    pool.submit(handle_connection, s, msg)
                else:
                    print(f'connection with {s.getpeername()} is closed')
                    s.close()

        for s in errors:
            print(f'connection with {s.getpeername()} has error')
            inputs.discard(s)
            s.close()
