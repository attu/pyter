import sys
from socket import socket


cli = socket()


port = int(sys.argv[1])
try:
    host = sys.argv[2]
except IndexError:
    host = 'localhost'
cli.connect((host, port))
print(f'client socket: {cli.getsockname()}')

msg = input('type message... ')
while msg:
    print(f'sending "{msg}" to {host}:{port}')
    cli.sendall(msg.encode('utf-8'))
    print('awaiting resp...')
    resp = cli.recv(1024)
    if not resp:
        break
    print(f'got resp "{resp}"')
    msg = input('type message... ')
