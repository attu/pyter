#include "client.hpp"
#include <thread>
#include <iostream>

using boost::asio::ip::tcp;

int main(int argc, char* argv[])
{
  try
  {
    if (argc != 3)
    {
      std::cerr << "Usage: chat_client <host> <port>\n";
      return 1;
    }

    boost::asio::io_service io_service;

    auto client = std::make_shared<xcode::client>(io_service);
    client->connect(argv[1], argv[2]);

    std::thread t([&io_service] { io_service.run(); });

    while (true)
    {
      client->write("lorem");
      auto const msg = client->read();
      std::cout << "got message: " << msg << "\n";
    }

    client->close();
    t.join();
  } catch (std::exception& e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}
