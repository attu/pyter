#include "client.hpp"

xcode::client::client(boost::asio::io_service& io_service) : ios(io_service), socket(io_service) {}

xcode::client::~client() noexcept
{
  close();
}

xcode::message xcode::client::read()
{
  return received.pop();
}

void xcode::client::connect(std::string host, std::string port)
{
  boost::asio::async_connect(socket, tcp::resolver(ios).resolve({host, port}),
                             [self = shared_from_this()](auto ec, auto) {
                               if (ec) return;
                               self->do_read();
                             });
}

void xcode::client::do_read()
{
  auto buffer = boost::asio::buffer(header.data(), header.size());
  socket.async_read_some(buffer, [self = shared_from_this()](auto ec, auto len) {
    if (ec) return self->close();
    message msg{self->header.data(), self->header.data() + len};
    try
    {
      self->received.push(std::move(msg));
    } catch (ClosedQueue const&)
    {
      return;
    }
    self->do_read();
  });
}

void xcode::client::write(message msg)
{
  ios.post([self = shared_from_this(), msg = std::move(msg)]() {
    bool write_in_progress = !self->outgoing.empty();
    self->outgoing.push_back(std::move(msg));
    if (!write_in_progress) self->do_write();
  });
}

void xcode::client::close()
{
  ios.post([self = shared_from_this()]() { self->socket.close(); });
  received.close();
}

void xcode::client::do_write()
{
  auto buffer = boost::asio::buffer(outgoing.front().data(), outgoing.front().size());
  boost::asio::async_write(socket, buffer, [self = shared_from_this()](auto ec, auto) {
    if (ec) return self->close();
    self->outgoing.pop_front();
    if (!self->outgoing.empty()) self->do_write();
  });
}
