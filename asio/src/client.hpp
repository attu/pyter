#pragma once

#include "synced_queue.hpp"
#include <boost/asio.hpp>
#include <functional>
#include <string>
#include <memory>
#include <deque>
#include <array>

namespace xcode
{
  using message = std::string;
  using boost::asio::ip::tcp;
  struct client : std::enable_shared_from_this<client> {
    client(boost::asio::io_service&);
    client(client const&) = default;
    client(client&&) = default;
    client& operator=(client const&) = default;
    client& operator=(client&&) = default;
    ~client();

    void connect(std::string, std::string);
    void write(message);
    message read();
    void close();

  private:
    void do_read();
    void do_write();

    boost::asio::io_service& ios;
    tcp::socket socket;
    std::array<message::value_type, 128> header;
    synced_queue<message> received;
    std::deque<message> outgoing;
  };
} // namespace xcode
