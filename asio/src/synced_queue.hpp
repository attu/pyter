#pragma once

#include <queue>
#include <chrono>
#include <mutex>
#include <exception>
#include <condition_variable>

namespace xcode
{
  struct QueueError : virtual std::exception {
  };

  struct ClosedQueue : virtual QueueError {
    inline const char* what() const noexcept override { return "Broken pipe"; }
  };

  struct Timeout : virtual QueueError {
    inline const char* what() const noexcept override { return "Timeout"; }
  };

  template <typename T>
  class synced_queue {
  public:
    using value_type = T;
    using reference = T&;
    using const_reference = T const&;

    synced_queue() = default;
    void push(value_type&&);
    void push(const_reference);

    template <typename... Args>
    reference emplace(Args&&...);

    value_type pop();

    template <typename Pred>
    value_type pop(Pred);

    template <typename Pred, typename Rep, typename Period>
    value_type pop(Pred, std::chrono::duration<Rep, Period> const&);

    template <typename Rep, typename Period>
    value_type pop(std::chrono::duration<Rep, Period> const&);

    void close();

  private:
    std::mutex mtx;
    std::condition_variable write;
    std::queue<value_type> queue;
    bool is_running = true;
  };
} // namespace xcode

template <typename T>
template <typename... Args>
auto xcode::synced_queue<T>::emplace(Args&&... args) -> reference
{
  std::unique_lock<std::mutex> lock(mtx);
  if (!is_running) throw ClosedQueue();
  queue.emplace(std::forward<Args>(args)...);
  write.notify_all();
  return queue.back();
}

template <typename T>
void xcode::synced_queue<T>::push(value_type&& value)
{
  emplace(std::move(value));
}

template <typename T>
void xcode::synced_queue<T>::push(const_reference value)
{
  emplace(value);
}

template <typename T>
template <typename Pred>
auto xcode::synced_queue<T>::pop(Pred pred) -> value_type
{
  std::unique_lock<std::mutex> lock(mtx);
  write.wait(lock, [this, pred = std::move(pred)] {
    return !is_running || (!queue.empty() && pred(queue.front()));
  });
  if (!is_running) throw ClosedQueue();
  auto value = std::move(queue.front());
  queue.pop();
  lock.unlock();
  write.notify_all();
  return value;
}

template <typename T>
template <typename Pred, typename Rep, typename Period>
auto xcode::synced_queue<T>::pop(Pred pred, std::chrono::duration<Rep, Period> const& timeout)
    -> value_type
{
  std::unique_lock<std::mutex> lock(mtx);
  if (!write.wait_for(lock, timeout, [this, pred = std::move(pred)] {
        return !is_running || (!queue.empty() && pred(queue.front()));
      }))
    throw Timeout();
  if (!is_running) throw ClosedQueue();
  auto value = std::move(queue.front());
  queue.pop();
  lock.unlock();
  write.notify_all();
  return value;
}

template <typename T>
auto xcode::synced_queue<T>::pop() -> value_type
{
  return pop([](auto const&) { return true; });
}

template <typename T>
template <typename Rep, typename Period>
auto xcode::synced_queue<T>::pop(std::chrono::duration<Rep, Period> const& timeout) -> value_type
{
  return pop([](auto const&) { return true; }, timeout);
}

template <typename T>
void xcode::synced_queue<T>::close()
{
  std::unique_lock<std::mutex> lock(mtx);
  is_running = false;
  write.notify_all();
}
